# cv-api


## Name
CV-API

## Description
This project is a small example of how a CV-API written in Python Flask could look like

## Installation
- disclaimer! This project was successfully tested on a unix device with python3.8 only!
- clone the repo
- navigate to the cv-api directory
- create a virtual environment: python3 -m venv ./venv
- activate the environment: source ./venv/bin/activate
- update pip: pip install --upgrade pip
- install the packages: pip install -r requirements.txt
- create a .env file similar with .env.example
- navigate to the cv-api/src directory
- run this command: gunicorn wsgi:app --workers=4 --bind=0.0.0.0:8000
- open a browser and go to: http://localhost:8000/cv/read/experience
- open another terminal and navigate again to cv-api/src
- run this command: flask read-info story
