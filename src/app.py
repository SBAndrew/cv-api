import os
from pathlib import Path
from dotenv import dotenv_values, load_dotenv
from flask import Flask
from routes.cv import blueprint_cv
from models.cv import read as model_cv_read
import click

env_path = Path(__file__).parent.parent / '.env'
load_dotenv(env_path)

app = Flask(__name__)
app.config.from_object('config')

app.register_blueprint(blueprint_cv, url_prefix='/cv')

@app.cli.command('read-info')
@click.argument('info_type')
def read_info(info_type):
    for batch in model_cv_read(info_type):
        print(batch)

def main():
    app.run(host=os.environ.get('FLASK_HOST', 'localhost'), port=os.environ.get('FLASK_PORT', '8000'))
  
if __name__ == '__main__':
    main()
