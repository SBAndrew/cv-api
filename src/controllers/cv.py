from flask import Response, abort
from models.cv import read as model_cv_read

def index():
    return {
        'status': 'OK',
        'response': 'pong',
    }

def read(info_type):
    response = model_cv_read(info_type)
    if not response:
        abort(404)
    return Response(model_cv_read(info_type), mimetype='text/plain')
