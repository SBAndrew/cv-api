from enum import Enum
from pathlib import Path
import time

def read(info_type: str, batch_size: int = 256) -> str:
    ''''''
    file_path = Path(__file__).parent.parent / 'data' / f'{info_type}.txt'

    if not file_path.exists():
        return False

    with open(file_path, 'r') as file:
        while True:
            batch = file.read(batch_size)
            if not batch:
                return
            # time.sleep(1) # uncomment this to see story loading in slow motion
            yield batch
