from flask import Blueprint
from controllers.cv import index as controller_cv_index
from controllers.cv import read as controller_cv_read

blueprint_cv = Blueprint('blueprint_cv', __name__)

blueprint_cv.route('/ping', methods=['GET'])(controller_cv_index)
blueprint_cv.route('/read/<string:info_type>', methods=['GET'])(controller_cv_read)
