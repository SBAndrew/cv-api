from app import app

def main():
    app.run()
    
if __name__ == '__main__':
    main()

# gunicorn wsgi:app --workers=4 --bind=0.0.0.0:8000 --chdir src
